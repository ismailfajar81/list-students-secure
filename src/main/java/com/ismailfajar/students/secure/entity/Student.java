package com.ismailfajar.students.secure.entity;


import lombok.Data;
import javax.persistence.*;

@Data
@Entity
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "firstname")
    private String firstName;

    @Column(name = "lastname")
    private String lastName;

    @Column(name = "kelas")
    private String kelas;

    @Column(name = "email")
    private String email;

    public Student() {
    }

    public Student(String firstName, String lastName, String kelas, String email) {
        super();
        this.firstName = firstName;
        this.lastName = lastName;
        this.kelas = kelas;
        this.email = email;
    }
}
