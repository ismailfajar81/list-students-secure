package com.ismailfajar.students.secure;

import com.ismailfajar.students.secure.entity.Student;
import com.ismailfajar.students.secure.repository.StudentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {

	private static final Logger log = LoggerFactory.getLogger(Application.class);
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Bean
	public CommandLineRunner demo(StudentRepository repository) {
		return (args) -> {
			// save students
			repository.save(new Student("Ismail","Fajar","X","ismailfajar81@gmail.com"));
			repository.save(new Student("Siti","Yuliatin","X","sitiyuliatin@gmail.com"));
			repository.save(new Student("Alina","Kasyifa","X","alinakasyifa@gmail.com"));
			repository.save(new Student("Farris","Muhammad","X","farrismuhammad@gmail.com"));
			repository.save(new Student("Azalia","Yasmina","X","azaliayasmina@gmail.com"));
			repository.save(new Student("Muallif","Zaaditaqwa","X","muallifzt@gmail.com"));
		};
	}
}
