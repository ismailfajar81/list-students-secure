package com.ismailfajar.students.secure.repository;

import com.ismailfajar.students.secure.entity.Student;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface StudentRepository extends CrudRepository<Student, Long> {
    List<Student> findByLastName(String lastname);
}
